/*
 * main.cpp
 *
 *  Created on: Sep 12, 2014
 *      Author: Batterypowered7
 */
#include "permutations.h"

/*
 * main.cpp
 *
 *  Created on: Sep 12, 2014
 *      Author: Batterypowered7
 */
#include "permutations.h"

int main()
{
   cout << "Declaring an array of integers." << endl;
   int a[] = {1, 2, 3, 4};
   int size = (sizeof(a)/sizeof(a[0]));
   cout << "Array a[] has: " << size << " elements." << endl;
   cout << "The elements in array a[] are: ";
   outputArray(a, size, cout);
   outputPermutations(a, size, cout);
   cout << endl;

   cout << "Declaring an array of characters." << endl;
   char b[] = {'A', 'B', 'C', 'D'};
   int sizeB = (sizeof(b)/sizeof(b[0]));
   cout << "Array b[] has: " << sizeB << " elements." << endl;
   cout << "The elements in array b[] are: ";
   outputArray(b, sizeB, cout);
   outputPermutations(b, sizeB, cout);
   cout << endl;

   cout << "Declaring an array of integers." << endl;
   int c[] = {1, 2, 3};
   int sizeC = (sizeof(c)/sizeof(c[0]));
   cout << "Array c[] has: " << sizeC << " elements." << endl;
   cout << "The elements in array c[] are: ";
   outputArray(c, sizeC, cout);
   outputPermutations(c, sizeC, cout);
   cout << endl;

   cout << "Declaring an array of characters." << endl;
   char d[] = {'A', 'B', 'C', 'D', 'E'};
   int sizeD = (sizeof(d)/sizeof(d[0]));
   cout << "Array d[] has: " << sizeD << " elements." << endl;
   cout << "The elements in array d[] are: ";
   outputArray(d, sizeD, cout);
   outputPermutations(d, sizeD, cout);
   cout << endl;
}
