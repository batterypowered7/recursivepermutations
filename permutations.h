#include <vector>
#include <string>
#include <iostream>
using namespace std;
#define START 0


#ifndef PERMUTATIONS_H_
#define PERMUTATIONS_H_

/************************
 * Function declarations
 ************************/
long factorial(const int& n);

template <class T>
void outputArray(T* items, const int& size, ostream& out);

template <class T>
void outputPermutations(T* items, const int& size, ostream& out);

template <typename T>
void permutations(T* input, const int& inputSize, int start, long& perms);

/***********************
 * Function definitions
 ***********************/

long factorial(const int& n)
{
	if(n < 2)
	{
		return 1;
	}

	return n * factorial(n - 1);
}

template <class T>
void outputArray(T* items, const int& size, ostream& out)
{
	for(int i = 0; i < size; ++i)
	{
		out << items[i] << " ";
	}
	out << endl;
}

template <class T>
void outputPermutations(T* items, const int& size, ostream& out)
{
	long targetPermutations = factorial(size);
	long permutationsCalculated = 0;
	permutations(items, size, START, permutationsCalculated);

	out << "Expected " << targetPermutations << " permutations\n" << "Actual "
	"number of permutations calculated: " << permutationsCalculated << endl;
}

template <typename T>
void permutations(T* input, const int& inputSize, int start, long& perms )
{
	   int j;
	   if (start == inputSize)
	   {
		   outputArray(input, inputSize, cout);
		   ++perms;
	   }
	   else
	   {
	       for (j = start; j < inputSize; j++)
	       {
	          swap(input[start], input[j]);
	          permutations(input, inputSize, start+1, perms);
	          swap(input[start], input[j]);
	       }
	   }
}

#endif /* PERMUTATIONS_H_ */
